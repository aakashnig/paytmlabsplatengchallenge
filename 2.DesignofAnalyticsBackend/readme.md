## Provided Requirements: 
1. Billion writes per day.
2. Millions of ready / query per day in form of time series. 
3. Provides metrics to customers with one-hour of delay
4. Reprocess historical data in the case of processing logic. 


## Design Decisions
1. Use Cassandra as primary data store. (Other Alernatives include DynamoDb, MongoDb)
2. Distribute Cassandra Cluster between multiple availability zone, multiple data centers.
3. Use a separate Cassandra store (with similar config), to provides reads to customers with one hour of delay.
4. Use Amazon Glacier for archiving data for historical storage. 
5. Provide REST APIs for query,read, and hisotorical access, and for providing metrics to the Customers. 

## Cassandra Configuration 
1. consistency level "ONE" which means that they are acknowledged when one node has acknowledged the data.
2. For consistent read after write data access our alternative pattern is to use "LOCAL QUORUM”: In that case the client acknowledgement waits for two out of the three nodes to acknowledge the data and the write response time increases slightly, but the work done by the Cassandra cluster is essentially the same
3. The client is writing 10 columns per row key, row key randomly chosen from 27 million ids, each column has a key and 10 bytes of data. 
4. The total on disk size for each write including all overhead is about 400 bytes

### Expected Writes Per Second (As per Cassandra Benchmarking***** )
1. 1 Million per second ; 86400 million per day;  86.4 billion per day. 
2. Since our requirements is 1 Billion per day, we can further fine tune this to get better read rate. 

# **** References for Casssandra Benchmarking: 
1. http://msrg.utoronto.ca/papers/NoSQLBenchmark (fun fact: I am coauthor with the author of this paper for their another project. This paper was submitted in front of me) 
2. http://techblog.netflix.com/2011/11/benchmarking-cassandra-scalability-on.html
3. https://academy.datastax.com/planet-cassandra/nosql-performance-benchmarks


## 1. Read/Query Requirements
1. Cassandra Query Language can be used to support Read Queries.  https://docs.datastax.com/en/cql/3.1/cql/cql_intro_c.html
2. No Benchmark exist for this, so we will have to benchmark. We can compromise on the write rates (86.4 billion), to get better read rates.  
3. Factors affecting Cassandra’s Read rate: Following are the factors that affect casandra read performance, and they can be futher fine tuned to improve the performance (1.Bloom Filter False Positive, 2:Consistency Level, 3. Read Repair Chanc, 4. Caching, 5.Compaction, 6.Data modeling. 7.Cluster Deployment)
4. We should provide Read/Query access to Cassandra cluster via a REST API. 

## 2. Read: Metrics to the Customers
1. We will maintain another Cassandra cluster to provide Metrics to the Customer. 
2. The read cluster will copy values from the main cluster, and will provide data to the customers behind a REST Based API. 
3. Maintaing two separate Cassandra Cluster for Read, and Write 


## 3. Read: Historical Storage. 
1. We will store all the historical data in Amazon Glacier (https://aws.amazon.com/glacier/?hp=tile&so-exp=below), and provide access to it via a REST API.
2. Amazon Glacier provides access from a few minutes to several hours. 


## High Level System Architecture
![High Level System Architecture](https://d3c9exejmch0fg.cloudfront.net/imageforppt.png)





